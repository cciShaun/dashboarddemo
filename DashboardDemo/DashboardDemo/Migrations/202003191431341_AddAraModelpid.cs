﻿namespace DashboardDemo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAraModelpid : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.AreaModels");
            AddColumn("dbo.AreaModels", "pid", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.AreaModels", "AreaName", c => c.String());
            AddPrimaryKey("dbo.AreaModels", "pid");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.AreaModels");
            AlterColumn("dbo.AreaModels", "AreaName", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.AreaModels", "pid");
            AddPrimaryKey("dbo.AreaModels", "AreaName");
        }
    }
}
