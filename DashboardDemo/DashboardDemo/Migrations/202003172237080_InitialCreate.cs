﻿namespace DashboardDemo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AreaModels",
                c => new
                    {
                        AreaName = c.String(nullable: false, maxLength: 128),
                        Temp = c.Int(nullable: false),
                        Humidity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AreaName);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AreaModels");
        }
    }
}
