﻿namespace DashboardDemo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CPUMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CPUModels",
                c => new
                    {
                        TimeStamp = c.String(nullable: false, maxLength: 128),
                        Cpu = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TimeStamp);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CPUModels");
        }
    }
}
