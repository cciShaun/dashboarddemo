﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DashboardDemo.Models
{
    public class AreaModel
    {
        [Key]
        public int pid { get; set; } = 0;
        public string AreaName { get; set; }
        public int Temp { get; set; }
        public int Humidity { get; set; }

    }
}