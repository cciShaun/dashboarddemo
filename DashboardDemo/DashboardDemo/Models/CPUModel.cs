﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DashboardDemo.Models
{
    public class CPUModel
    {
        [Key]
        public string TimeStamp { get; set; }

        public int Cpu { get; set; }
    }
}