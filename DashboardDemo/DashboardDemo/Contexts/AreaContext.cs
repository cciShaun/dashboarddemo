﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using DashboardDemo.Models;


namespace DashboardDemo.Contexts
{
    public class AreaContext : DbContext
    {
        public AreaContext() : base()
        {

        }

        public DbSet<AreaModel> Areas { get; set; }
        public DbSet<CPUModel> CPUs { get; set; }
    }
}