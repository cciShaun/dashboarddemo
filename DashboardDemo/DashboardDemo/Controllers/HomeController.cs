﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DashboardDemo.Models;

namespace DashboardDemo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            using (var context = new Contexts.AreaContext())
            {
                /*
                string[] regions = { "Liberia", "Niue", "Guam", "Vanuatu", "Guinea-Bissau", "Saint Pierre and Miquelon", "Saint BarthÃ©lemy", "Cameroon", "Korea, North", "Reunion", "Guatemala", "Algeria", "Guernsey", "New Zealand", "Saint Pierre and Miquelon", "Gambia", "British Indian Ocean Territory", "Cocos (Keeling) Islands", "South Georgia and The South Sandwich Islands", "Gabon", "Gibraltar", "Syria", "Colombia", "Cook Islands", "Mauritania", "Nepal", "South Georgia and The South Sandwich Islands", "Honduras", "Croatia", "Malaysia", "Serbia", "Guernsey", "Liberia", "Congo (Brazzaville)", "Guinea", "Hungary", "Guinea-Bissau", "Aruba", "Mauritania", "Tajikistan", "Japan", "Burkina Faso", "Tonga", "San Marino", "Mauritius", "Mali", "Armenia", "Honduras", "France", "Poland", "Colombia", "Spain", "Barbados", "Mauritania", "Cuba", "Bouvet Island", "Macedonia", "Spain", "Saint Lucia", "Mauritius", "Palau", "Moldova", "Anguilla", "Guernsey", "Mali", "Niger", "Pakistan", "French Guiana", "Netherlands", "Germany", "United States Minor Outlying Islands", "Japan", "Afghanistan", "Niue", "CÃ´te D'Ivoire (Ivory Coast)", "Fiji", "Faroe Islands", "Sao Tome and Principe", "Serbia", "Turkey", "Somalia", "Rwanda", "Niue", "Cocos (Keeling) Islands", "Hong Kong", "Gabon", "Bermuda", "Christmas Island", "Guam", "Bermuda", "Korea, North", "Seychelles", "Costa Rica", "Micronesia", "Armenia", "Malawi", "Virgin Islands, United States", "Virgin Islands, United States", "Malta", "Guatemala" };
                int[] temps = { -34, 51, 87, 3, 67, 20, 74, 120, -37, 57, -34, 113, 115, 81, 12, 116, 93, 30, 97, 95, 21, -33, 13, -24, -6, 18, -31, 100, 64, 16, 51, 89, -20, 24, 32, 78, -21, -5, 118, -3, 117, 54, 90, -31, 21, 96, 61, 24, 78, 15, 64, 23, 83, 20, 111, 82, 41, 75, 91, 111, 14, 41, 110, -8, -33, 82, -33, 91, -21, 115, 12, 30, 47, 105, -7, 112, 21, 30, 1, 22, 85, 64, 27, 72, 70, -12, 36, -1, 80, -27, 29, -3, 30, 29, -9, -31, 21, 102, 32, -38 };
                int[] hums = { 57, 72, 67, 36, 17, 25, 42, 97, 62, 3, 74, 71, 17, 3, 7, 55, 3, 45, 50, 56, 86, 78, 19, 94, 76, 55, 56, 78, 16, 67, 74, 1, 95, 36, 76, 89, 62, 78, 91, 78, 85, 39, 27, 46, 37, 48, 71, 47, 35, 98, 55, 30, 57, 37, 90, 92, 41, 85, 24, 86, 62, 33, 19, 12, 89, 11, 65, 67, 88, 50, 21, 46, 96, 7, 92, 28, 65, 8, 5, 84, 24, 44, 41, 86, 21, 84, 31, 90, 63, 95, 24, 19, 5, 15, 13, 81, 3, 76, 10, 43 };

                for (int i = 0; i <= regions.Length; i++)
                {
                    AreaModel area = new AreaModel() { AreaName = regions[i], Temp = temps[i], Humidity = hums[i] };

                    try
                    {
                        context.Areas.Add(area);
                        context.SaveChanges();
                    }
                    catch
                    {

                    }
                }
                */
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}