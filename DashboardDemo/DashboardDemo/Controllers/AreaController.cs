﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DashboardDemo.Contexts;
using DashboardDemo.Models;
using Newtonsoft.Json;

namespace DashboardDemo.Controllers
{
    public class AreaController : Controller
    {
        private AreaContext db = new AreaContext();

        // GET: Area
        public ActionResult Index()
        {
            AreaModel[] areas = db.Areas.ToArray();
            string[] areaNames = new string[areas.Length];
            int[] areaTemps = new int[areas.Length];
            int[] areaHumidity = new int[areas.Length];

            for (int i = 0; i < areas.Length; i++)
            {
                areaNames[i] = areas[i].AreaName;
                areaTemps[i] = areas[i].Temp;
                areaHumidity[i] = areas[i].Humidity;
            }
            ViewBag.NAMES = areaNames;
            ViewBag.TEMPS = areaTemps;
            ViewBag.HUMIDITY = areaHumidity;

            IEnumerable<CPUModel> query = db.CPUs.AsEnumerable()
                                         .Where(cpu => cpu.TimeStamp.Split(' ')[0] == DateTime.Now.ToString().Split(' ')[0])//.OrderByDescending(t => t.TimeStamp)
                                         .Take(50);

            CPUModel[] cPUs = query.ToArray();
            string[] timeStamps = new string[cPUs.Length];
            int[] cpus = new int[cPUs.Length];

            for (int i = 0; i < cPUs.Length; i++)
            {
                timeStamps[i] = cPUs[i].TimeStamp;
                cpus[i] = cPUs[i].Cpu;
            }

            ViewBag.TIMESTAMPS = timeStamps;
            ViewBag.CPUS = cpus;
            ViewBag.COUNT = cPUs.Length;

            //Response.AddHeader("Refresh", "5");
            return View();
        }

        public ContentResult PageInfo()
        {
            IEnumerable<CPUModel> query = db.CPUs.AsEnumerable()
                                         .Where(cpu => DateTime.Parse(cpu.TimeStamp) > DateTime.Now.AddMinutes(-5));


            List<string> timeStamps = new List<string>();
            List<int> cpus = new List<int>();
            List<CPUModel> cPUs = query.ToList();
            foreach (CPUModel model in cPUs)
            {

                timeStamps.Add(model.TimeStamp);
                cpus.Add(model.Cpu);
            }

            dataReturn data = new dataReturn();
            data.TimeStamp = timeStamps.ToArray();
            data.Data = cpus.ToArray();

            var json = JsonConvert.SerializeObject(data);
            return Content(json);
        }

       
    }
}

public class dataReturn
{
    public object[] TimeStamp { get; set; }
    public int[] Data { get; set; }
}
